using System;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OpenAmp
{
    class RPCBase
    {
        [JsonPropertyName("jsonrpc")]
        public String Version { get; set; }
    }

    class RPCRequest : RPCBase
    {
        [JsonPropertyName("method")]
        public String Method { get; set; }

        [JsonPropertyName("params")]
        public object[] Params { get; set; }

        public String Serialize()
        {
            return new StringBuilder(JsonSerializer.Serialize(this)).AppendLine().ToString();
        }

        public RPCRequest(String method, Object[] parameter)
        {
            this.Version = "2.0";
            this.Method = method;
            this.Params = parameter;
        }
    }

    class RPCResponse<T> : RPCBase
    {
        [JsonPropertyName("result")]
        public T Result { get; set; }
    }


}
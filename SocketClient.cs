using System;
using System.Net;
using System.Net.Sockets;

namespace OpenAmp
{
    class SocketClient
    {
        String _serverUrl;
        Int32 _port;
        Socket _socket;

        public void Connect()
        {
            IPHostEntry hostEntry = null;

            // Get host related information.
            hostEntry = Dns.GetHostEntry(_serverUrl);

            // Loop through the AddressList to obtain the supported AddressFamily. This is to avoid
            // an exception that occurs when the host IP Address is not compatible with the address family
            // (typical in the IPv6 case).
            foreach (IPAddress address in hostEntry.AddressList)
            {
                IPEndPoint ipe = new IPEndPoint(address, _port);
                Socket tempSocket =
                    new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                tempSocket.Connect(ipe);

                if (tempSocket.Connected)
                {
                    _socket = tempSocket;
                    break;
                }
            }
        }

        public Byte[] SendReceiveMessage(Byte[] message)
        {
            Byte[] bytesReceived = new Byte[4096];
            _socket.Send(message, message.Length, 0);
            int requestStatus;
            requestStatus = _socket.Receive(bytesReceived);
            return bytesReceived;
        }


        public void SendMessage(Byte[] message)
        {
            _socket.Send(message, message.Length, 0);
        }
        public void Close()
        {
            _socket.Close();
        }

        public SocketClient(String serverUrl, Int32 port)
        {
            _serverUrl = serverUrl;
            _port = port;
        }

    }
}
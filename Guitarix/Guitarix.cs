using System.Text;
using System.Text.Json;
using OpenAmp;

namespace Guitarix
{
    class GuitarixClient
    {

        private SocketClient _client;
        /* public AmplifierHeadModel GetAllAmplifierHeadValues()
         {
             var request3 = "{\"method\":\"get_parameter\",\"params\":[\"amp.out_master\", \"amp2.stage1.Pregain\", \"amp2.stage2.gain1\", \"gxdistortion.drive\", \"gxdistortion.wet_dry\", \"tube.select\", \"bassbooster.Level\", \"con.Level\", \"amp.wet_dry\", \"amp.bass_boost.on_off\", \"con.on_off\", \"amp.feed_on_off\"],\"id\":1,\"jsonrpc\":\"2.0\"}\n";
             var message = _client.SendMessage(Encoding.ASCII.GetBytes(request3));
             return JsonSerializer.Deserialize<RPCResponse<AmplifierHeadModel>>(Encoding.ASCII.GetString(message).Replace("\0", string.Empty)).Result;
         }
 */
        public AmplifierHead.MasterVolume MasterVolume;
        public AmplifierHead.BassBoost BassBoost;
        public AmplifierHead.CleanDistorted CleanDistorted;
        public AmplifierHead.Drive Drive;
        public AmplifierHead.Gain Gain;
        public AmplifierHead.Pregain Pregain;
        public AmplifierHead.Presence Presence;
        public AmplifierHead.Reverb Reverb;


        public GuitarixClient(SocketClient client)
        {
            _client = client;
            MasterVolume = new AmplifierHead.MasterVolume(client);
            BassBoost = new AmplifierHead.BassBoost(client);
            CleanDistorted = new AmplifierHead.CleanDistorted(client);
            Drive = new AmplifierHead.Drive(client);
            Gain = new AmplifierHead.Gain(client);
            Pregain = new AmplifierHead.Pregain(client);
            Presence = new AmplifierHead.Presence(client);
            Reverb = new AmplifierHead.Reverb(client);
        }
    }
}
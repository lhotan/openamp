using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Guitarix.Models
{
    class GuitarixValueBase<T>
    {
        // 12 members in tube 
        [JsonPropertyName("group")]
        public string Group { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        // TODO: figure out how unions work in C#
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("value")]
        public T Value { get; set; }
    }

    class GuitarixValueContinuous<T> : GuitarixValueBase<T>
    {
        [JsonPropertyName("ctl_continous")]
        public double Continous { get; set; }
        [JsonPropertyName("upper_bound")]
        public double UpperBound { get; set; }
        [JsonPropertyName("lower_bound")]
        public double LowerBound { get; set; }

        [JsonPropertyName("step")]
        public double Step { get; set; }
    }
    class GuitarixValueSwitch<T> : GuitarixValueBase<T>
    {
        [JsonPropertyName("ctl_switch")]
        public double Switch { get; set; }
    }

    class GuitarixValueEnum<T> : GuitarixValueBase<T>
    {
        [JsonPropertyName("ctl_enum")]
        public double Enum { get; set; }

        [JsonPropertyName("value_names")]
        public List<string[]> ValueNames { get; set; }
    }
}
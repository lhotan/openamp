using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class MasterVolume
    {
        private SocketClient _client;

        public void SetMasterVolume(double value)
        {
            var mapped = Program.Map(value, 0, 100, -50, 4);
            System.Console.WriteLine(mapped);
            var request = new RPCRequest("set", new Object[] { Json.MasterVolume, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public MasterVolume(SocketClient client)
        {
            _client = client;
        }
    }
}
using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class Pregain
    {
        private SocketClient _client;

        public void SetPregain(double value)
        {
            var mapped = Program.Map(value, 0, 100, -20, 20);
            var request = new RPCRequest("set", new Object[] { Json.Pregain, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public Pregain(SocketClient client)
        {
            _client = client;
        }
    }
}
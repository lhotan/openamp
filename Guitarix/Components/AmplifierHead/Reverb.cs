using System;
using System.Text;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class Reverb
    {
        private SocketClient _client;

        public void SetReverb(double value)
        {
            var mapped = Program.Map(value, 0, 100, -1, 1);
            var request = new RPCRequest("set", new Object[] { Json.Reverb, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public Reverb(SocketClient client)
        {
            _client = client;
        }
    }
}
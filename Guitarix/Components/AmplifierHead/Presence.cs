using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class Presence
    {
        private SocketClient _client;

        public void SetPresence(double value)
        {
            var mapped = Program.Map(value, 0, 100, 0.0, 5.0);
            var request = new RPCRequest("set", new Object[] { Json.Presence, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public Presence(SocketClient client)
        {
            _client = client;
        }
    }
}
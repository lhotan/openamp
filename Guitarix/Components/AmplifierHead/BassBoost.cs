using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class BassBoost
    {
        private SocketClient _client;

        public void SetBassBoost(double value)
        {
            var mapped = Program.Map(value, 0, 100, 0, 20);
            var request = new RPCRequest("set", new Object[] { Json.BassBoost, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public BassBoost(SocketClient client)
        {
            _client = client;
        }
    }
}
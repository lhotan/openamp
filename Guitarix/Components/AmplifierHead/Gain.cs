using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class Gain
    {
        private SocketClient _client;

        public void SetGain(double value)
        {
            var mapped = Program.Map(value, 0, 100, -20, 20);
            var request = new RPCRequest("set", new Object[] { Json.Gain, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public Gain(SocketClient client)
        {
            _client = client;
        }
    }
}
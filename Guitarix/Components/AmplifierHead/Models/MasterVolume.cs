using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    class MasterVolumeValue
    {
        [JsonPropertyName(Json.MasterVolume)]
        public double Level { get; set; }
    }
    class MasterVolume : GuitarixValueSwitch<MasterVolumeValue> { }
}
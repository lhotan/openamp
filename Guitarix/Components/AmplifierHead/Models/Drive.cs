using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    class DriveValue
    {
        [JsonPropertyName(Json.Drive)]
        public double Level { get; set; }
    }
    class Drive : GuitarixValueSwitch<DriveValue> { }
}
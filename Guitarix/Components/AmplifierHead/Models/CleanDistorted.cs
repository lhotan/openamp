using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    class CleanDistortedValue
    {
        [JsonPropertyName(Json.CleanDistorted)]
        public double Level { get; set; }
    }
    class CleanDistorted : GuitarixValueSwitch<CleanDistortedValue> { }
}
namespace Guitarix.AmplifierHead.Models
{
    class Json
    {
        public const string MasterVolume = "amp.out_master";
        public const string Pregain = "amp2.stage1.Pregain";
        public const string Gain = "amp2.stage2.gain1";
        public const string Drive = "gxdistortion.drive";
        public const string CleanDistorted = "gxdistortion.wet_dry";
        public const string Tube = "tube.select";
        public const string BassBoost = "bassbooster.Level";
        public const string BassBoostEnabled = "amp.bass_boost.on_off";
        public const string Presence = "con.Level";
        public const string PresenceEnabled = "con.on_off";
        public const string Reverb = "amp.wet_dry";
        public const string ReverbEnabled = "amp.feed_on_off";
    }
}

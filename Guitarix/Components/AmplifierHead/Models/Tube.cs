using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    class TubeValue
    {
        [JsonPropertyName(Json.Tube)]
        public string Tube { get; set; }
    }
    class Tube : GuitarixValueEnum<TubeValue> { }
}
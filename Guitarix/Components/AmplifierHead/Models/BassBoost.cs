using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    internal class BassBoostEnabledValue
    {
        [JsonPropertyName(Json.BassBoostEnabled)]
        public int Enabled { get; set; }
    }
    class BassBoostEnabled : GuitarixValueSwitch<BassBoostEnabledValue> { }

    class BassBoostValue
    {
        [JsonPropertyName(Json.BassBoost)]
        public double Level { get; set; }
    }

    class BassBoost : GuitarixValueContinuous<BassBoostValue> { }
}
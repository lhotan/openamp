using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    class GainValue
    {
        [JsonPropertyName(Json.Gain)]
        public double Level { get; set; }
    }
    class Gain : GuitarixValueSwitch<GainValue> { }
}

using System.Text.Json.Serialization;

namespace Guitarix.AmplifierHead.Models
{
    class AmplifierHeadModel
    {
        [JsonPropertyName(Json.MasterVolume)]
        public MasterVolume MasterVolume { get; set; }

        [JsonPropertyName(Json.Pregain)]
        public Pregain Pregain { get; set; }

        [JsonPropertyName(Json.Gain)]
        public Gain Gain { get; set; }

        [JsonPropertyName(Json.Drive)]
        public Drive Drive { get; set; }

        [JsonPropertyName(Json.CleanDistorted)]
        public CleanDistorted CleanDistorted { get; set; }

        [JsonPropertyName(Json.Tube)]
        public Tube Tube { get; set; }

        [JsonPropertyName(Json.BassBoost)]
        public BassBoost BassBoost { get; set; }

        [JsonPropertyName(Json.BassBoostEnabled)]
        public BassBoostEnabled BassBoostEnabled { get; set; }

        [JsonPropertyName(Json.Presence)]
        public Presence Presence { get; set; }

        [JsonPropertyName(Json.PresenceEnabled)]
        public PresenceEnabled PresenceEnabled { get; set; }

        [JsonPropertyName(Json.Reverb)]
        public Reverb Reverb { get; set; }

        [JsonPropertyName(Json.ReverbEnabled)]
        public ReverbEnabled ReverbEnabled { get; set; }
    }
}

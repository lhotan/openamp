using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    class PregainValue
    {
        [JsonPropertyName(Json.Pregain)]
        public double Level { get; set; }
    }
    class Pregain : GuitarixValueSwitch<PregainValue> { }
}
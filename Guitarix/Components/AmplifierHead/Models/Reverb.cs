using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    internal class ReverbEnabledValue
    {
        [JsonPropertyName(Json.ReverbEnabled)]
        public int Enabled { get; set; }
    }
    class ReverbEnabled : GuitarixValueSwitch<ReverbEnabledValue> { }

    class ReverbValue
    {
        [JsonPropertyName(Json.ReverbEnabled)]
        public double Level { get; set; }
    }

    class Reverb : GuitarixValueContinuous<ReverbValue> { }
}
using System.Text.Json.Serialization;
using Guitarix.Models;

namespace Guitarix.AmplifierHead.Models
{
    internal class PresenceEnabledValue
    {
        [JsonPropertyName(Json.PresenceEnabled)]
        public int Enabled { get; set; }
    }
    class PresenceEnabled : GuitarixValueSwitch<PresenceEnabledValue> { }

    class PresenceValue
    {
        [JsonPropertyName(Json.Presence)]
        public double Level { get; set; }
    }

    class Presence : GuitarixValueContinuous<PresenceValue> { }
}
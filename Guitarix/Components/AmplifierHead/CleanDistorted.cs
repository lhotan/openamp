using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class CleanDistorted
    {
        private SocketClient _client;

        public void SetCleanDistorted(double value)
        {
            var request = new RPCRequest("set", new Object[] { Json.CleanDistorted, value }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public CleanDistorted(SocketClient client)
        {
            _client = client;
        }
    }
}
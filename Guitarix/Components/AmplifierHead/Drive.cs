using System;
using System.Text;
using System.Text.Json;
using Guitarix.AmplifierHead.Models;
using OpenAmp;

namespace Guitarix.AmplifierHead
{
    class Drive
    {
        private SocketClient _client;

        public void SetDrive(double value)
        {
            var mapped = Program.Map(value, 0, 100, 0.0, 1.0);
            var request = new RPCRequest("set", new Object[] { Json.Drive, mapped }).Serialize();
            _client.SendMessage(Encoding.ASCII.GetBytes(request));
        }

        public Drive(SocketClient client)
        {
            _client = client;
        }
    }
}
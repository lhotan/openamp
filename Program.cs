﻿using System;
using System.Text;
using System.Threading;
using System.Text.Json.Serialization;
using System.Text.Json;
using Guitarix;

namespace OpenAmp
{
    class Program
    {
        public static Double Map(Double s, Double a1, Double a2, Double b1, Double b2)
        {
            return Math.Round(b1 + (s - a1) * (b2 - b1) / (a2 - a1), 2);
        }

        static void Main(string[] args)
        {
            var client = new SocketClient(Config.ServerURL, Config.ServerPort);
            var guitarix = new GuitarixClient(client);

            client.Connect();


            while (true)
            {
                for (int i = 0; i < 100; i++)
                {
                    guitarix.MasterVolume.SetMasterVolume(i);
                    guitarix.BassBoost.SetBassBoost(i);
                    guitarix.CleanDistorted.SetCleanDistorted(i);
                    guitarix.Drive.SetDrive(i);
                    guitarix.Gain.SetGain(i);
                    guitarix.Pregain.SetPregain(i);
                    guitarix.Presence.SetPresence(i);
                    guitarix.Reverb.SetReverb(i);
                    Thread.Sleep(50);
                }
                for (int i = 100; i > 0; i--)
                {
                    guitarix.MasterVolume.SetMasterVolume(i);
                    guitarix.BassBoost.SetBassBoost(i);
                    guitarix.CleanDistorted.SetCleanDistorted(i);
                    guitarix.Drive.SetDrive(i);
                    guitarix.Gain.SetGain(i);
                    guitarix.Pregain.SetPregain(i);
                    guitarix.Presence.SetPresence(i);
                    guitarix.Reverb.SetReverb(i);
                    Thread.Sleep(50);
                }
            }


            client.Close();
        }
    }
}